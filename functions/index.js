const functions = require('firebase-functions');

// later for storing SF org ID in Firebase
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const ROOT_URL = 'https://kipuapi.kipuworks.com';
const ACCEPT_HEADER_VALUE = 'application/vnd.kipusystems+json; version=1';
const request = require('request');
const md5Helper = require('./md5');
const sha1Hmac = require('hmacsha1');
const crypto = require('crypto');

exports.sendDummy = functions.https.onRequest((req,res)=>{
    res.send('ok');
});

function createReq(uri, body, credObj){
    let md5Digest = crypto.createHash('md5').update(body).digest('binary');

    // md5 should be blob, not string
    // let md5Digest = md5Helper.md5(body);
    // confirmed that it should be d41d8cd98f00b204e9800998ecf8427e

    // 24 character long Base64 binary encoded string
    // e.g. rqIfcOSoNaO29ZPBn7E/iQ==
    // (encode empty string ‘’ for GET endpoints)


    let contentStr = Buffer.from(md5Digest, 'binary').toString('base64', 0, 24);
    // confirmed that it should be

    let dateStr = new Date().toString().replace(/\+.+/, '');
    var canonicalStr = ','+contentStr+','+uri+','+dateStr;
    var signature = crypto.createHmac('sha1', canonicalStr).update(credObj.secretKey).digest('base64');

    return {
        url: ROOT_URL+uri,
        headers: {
            'Accept': ACCEPT_HEADER_VALUE,
            'Authorization': 'APIAuth '+credObj.accessId+':'+signature,
            'Content-MD5': contentStr,
            'Date': dateStr
        }
    }
}

exports.getPatients = functions.https.onRequest((req, res) => {
    let sfId = req.query.sfid;

    admin.database().ref('orgIds/'+sfId).once('value').then(snapshot => {
        let data = snapshot.val();
        return {
            accessId: data.accessId,
            appId: data.appId,
            secretKey: data.secretKey
        }
    }).then(credObj => {
        let phiLevel = 'medium';
        var uri = '/api/patients/occupancy?app_id='+credObj.appId+'&phi_level='+phiLevel;
        var reqObj = createReq(uri, '', credObj);
        return reqObj;
    }).then(reqObj => {
        // res.send(reqObj);
        request.get(reqObj, function(error, response, body){
            if (error){
                res.send(error);
            } else if (!error && response.statusCode === 200){
                // res.send('ok');
                res.send(response);
                // res.send(body);
            } else {
                res.send(body);
            }
        });
    }).catch(err => {
        res.send(err);
    });

});
